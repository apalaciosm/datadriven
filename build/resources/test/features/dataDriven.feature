#Autor  : Andres Palacios

  Feature:
    Como un usuario
    Quiero ingresar a Google Translator
    A traducir palabra entre diferentes lenguajes

    @traducir:
    Scenario Outline: Traducir de Inglés a Español
      Given que Andres quiere usar el traductor de google
      When  el traduce una palabra de inglés a español
        |palabrai  |
        |<palabrai>|

      Then  el deberia ver la <palabrae> traducida del idioma origen a idioma destino
        |palabrae  |
        |<palabrae>|
 Examples:
    |palabrai |palabrae |
    |table    |ESPAÑOL  |
    |house    |ESPAÑOL  |
