package co.com.choucair.certification.datadriventrans.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://translate.google.com")
public class googleTranslatePage extends PageObject {
    public static final Target INPUT_WORD = Target.the("field to the write word")
            .located(By.xpath("//*[@id='source']"));
    public static final Target ENG_BUTTON = Target.the("select english languaje")
            .located(By.xpath("//*[@id='sugg-item-en']"));
    public static final Target SPA_BUTTON = Target.the("select traducir al español ")
            .located(By.xpath("//*[@id='coursesearchbox']"));
    public static final Target OUTPUT_WORD = Target.the(" traducción al español ")
            .located(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/div"));





}
