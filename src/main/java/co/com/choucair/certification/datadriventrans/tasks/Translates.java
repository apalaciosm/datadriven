package co.com.choucair.certification.datadriventrans.tasks;

import co.com.choucair.certification.datadriventrans.model.googleTranslateData;
import co.com.choucair.certification.datadriventrans.userinterface.googleTranslatePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

public class Translates implements Task {
    private List<googleTranslateData> googletranslatedata;

    public Translates(List<googleTranslateData> googletranslatedata) {
        this.googletranslatedata = googletranslatedata;
    }

    public static Translates the(List<googleTranslateData> googletranslatedata) {
        return Tasks.instrumented(Translates.class,googletranslatedata);
    }

    public  <T extends Actor> void performAs(T actor) {

                   actor.attemptsTo(Enter.theValue(googletranslatedata.get(0).getPalabrai()).into(googleTranslatePage.INPUT_WORD)
                ,Click.on(googleTranslatePage.ENG_BUTTON)
                ,Click.on(googleTranslatePage.SPA_BUTTON)
        );
    }
}
