package co.com.choucair.certification.datadriventrans.questions;

import co.com.choucair.certification.datadriventrans.model.googleTranslateData;
import co.com.choucair.certification.datadriventrans.userinterface.googleTranslatePage;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import cucumber.api.java.en_scouse.An;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class Answer implements Question<Boolean> {
    private List<googleTranslateData> googletranslatedata;

    public Answer (List<googleTranslateData> googletranslatedata){
        this.googletranslatedata = googletranslatedata;

    }
    public static Answer toThe(List<googleTranslateData> googletranslatedata) {
        return new Answer(googletranslatedata);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String engtext= Text.of(googleTranslatePage.OUTPUT_WORD).viewedBy(actor).asString();
        System.out.println(engtext);
        System.out.println(googletranslatedata.get(0).getPalabrae());
        if(googletranslatedata.get(0).getPalabrae().equals(engtext)) {
            result = true;
        }else {
            result = false;
        }
        return  result;
    }
}
