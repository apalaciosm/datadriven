package co.com.choucair.certification.datadriventrans.stepdefinitions;

import co.com.choucair.certification.datadriventrans.model.googleTranslateData;
import co.com.choucair.certification.datadriventrans.tasks.OpenUp;
import co.com.choucair.certification.datadriventrans.tasks.Translates;
import co.com.choucair.certification.datadriventrans.questions.Answer;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import java.util.List;

public class translatestepDefinitions {
    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que Andres quiere usar el traductor de google$")
    public void queAndresQuiereUsareltraductorDeGoogle()  {
            OnStage.theActorCalled("Andres").wasAbleTo(OpenUp.thePage());
    }


    @When("^el traduce una palabra de inglés a español$")
    public void elTraduceUnaPalabraDeInglésAEspañol(List<googleTranslateData> googletranslateData)  {
        OnStage.theActorInTheSpotlight().attemptsTo(Translates.the(googletranslateData));
    }

    @Then("^el deberia ver la palabra traducida del idioma origen a idioma destino$")
    public void elDeberiaVerLaPalabraTraducidaDelIdiomaOrigenAIdiomaDestino(List<googleTranslateData> googletranslateData)  {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(googletranslateData)));

    }


}
